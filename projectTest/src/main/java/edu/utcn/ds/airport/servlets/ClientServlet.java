package edu.utcn.ds.airport.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.utcn.ds.airport.dao.FligthDao;
import edu.utcn.ds.airport.models.Flight;

/**
 * Servlet implementation class ClientServlet
 */
@WebServlet("/ClientServlet")
public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println(request.getSession().getAttribute("userName")+" "+request.getSession().getAttribute("userType"));
		FligthDao dao = new FligthDao();
		List<Flight> flights = dao.findFlights();
		StringBuilder buf = new StringBuilder();
		buf.append("<html>" +
		           "<body background = \"D:\\an4_sem1\\DS_Assigment_01\\ds_30645_pal-magosandrea_assigment_1\\projectTest\\sky.JPG\">" +
				   "<h1 style=\"font-family:Comic Sans Ms; text-align: center; margin-top : 70px\">Hello!</h1>"+
		           "<table border=\"1\" align =\"center\" style=\"font-family:Comic Sans Ms; text-align: center; margin-top : 70px\" >" +
		           "<tr>" +
		           "<th>Flight Number</th>" +
		           "<th>Flight Type</th>" +
		           "<th>Deparute City</th>" +
		           "<th>Deparute Date</th>" +
		           "<th>Arrival City</th>" +
		           "<th>Arrival Date</th>" +
		           "</tr>");
		for (Flight f : flights) {
		    buf.append("<tr><td>")
		       .append(f.getFlightNumber())
		       .append("</td><td>")
		       .append(f.getType())
		       .append("</td><td>")
		       .append(f.getDepartureCity())
		       .append("</td><td>")
		       .append(f.getDepartureDate())
		       .append("</td><td>")
		       .append(f.getArrivalCity())
		       .append("</td><td>")
		       .append(f.getArrivalDate())
		       .append("</td></tr>");
		}
		buf.append("</table>" +
		           "</body>" +
		           "</html>");
		String html = buf.toString();
		PrintWriter pw = response.getWriter();
		pw.print(html);
		request.getSession().setAttribute("userName", null);       
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
