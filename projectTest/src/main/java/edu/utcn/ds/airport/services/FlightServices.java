package edu.utcn.ds.airport.services;

import java.sql.Date;

import edu.utcn.ds.airport.dao.FligthDao;
import edu.utcn.ds.airport.models.Flight;

public class FlightServices {
	
	public String addFlight(int flightNumber, String flightType,String deptCity, Date deptTime, String arrivCity, Date arrivTime) {
		 String response;
		
		FligthDao dao = new FligthDao();
		Flight flight = dao.addFlight(flightNumber, flightType, deptCity, deptTime, arrivCity, arrivTime);
		
		if(flight != null) {
		
            response = "sucessfull insert"+"\n"+flight.toString();
        } else {
            response = "fail in INSERT";
		}
		return response;
	}
	
	public String editFlight(int flightNumber, String flightType,String deptCity, Date deptTime, String arrivCity, Date arrivTime) {
		String response;
		
		FligthDao dao = new FligthDao();
		Flight flight = dao.findFlight(flightNumber);
		Flight f = dao.editFlight(flight, flightType, deptCity, deptTime, arrivCity, arrivTime);
		if(f != null) {
			response = "sucessfull update"+f.toString();
		}
		else {
			response = "fail in UPDATE";
		}
		
		return response;
	}
	
	
	public String deleteFlight(int flightNumber) {
		String response;
		
		FligthDao dao = new FligthDao();
		Flight flight = dao.findFlight(flightNumber);
		Flight f = dao.deleteFlight(flight);
		if(f != null) {
			response = "sucessfull delete"+"\n"+f.toString();
		}
		else {
			response = "fail in DELETE";
		}
		
		return response;
	}
	
	public String getAFlight(int flightNumber) {
		String response;
		FligthDao dao = new FligthDao();
		Flight f = dao.findFlight(flightNumber);
		if(f != null) {
			response = "sucessfull Read"+"\n"+f.toString();
		}
		else {
			response = "fail in GET";
		}
		
		return response;
	}

}
