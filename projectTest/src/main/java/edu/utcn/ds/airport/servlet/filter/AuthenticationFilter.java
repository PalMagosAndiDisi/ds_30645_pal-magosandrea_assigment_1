package edu.utcn.ds.airport.servlet.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AuthenticationFilter implements Filter {

	
	public void destroy() {
		// TODO Auto-generated method stub
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        if((!(req.getRequestURI().toString().contains("Hello")) && req.getSession().getAttribute("userName") == null) ||
        		(req.getRequestURI().toString().contains("Client") && req.getSession().getAttribute("userName") != null
        		&& req.getSession().getAttribute("userType").equals("admin"))) {
        	System.out.println(req.getSession().getAttribute("userType"));
        	res.sendRedirect("Hello.html");
        } else {
            // pass the request along the filter chain
            chain.doFilter(request, response);
        }
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
