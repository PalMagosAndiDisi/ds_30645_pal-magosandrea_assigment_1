package edu.utcn.ds.airport.models;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "FLIGHTS")
public class Flight {
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private int flightId;

	@Column(name = "flightNumber", nullable = false)
	private int flightNumber;

	@Column(name = "type", nullable = false)
	private String type;

	@Column(name = "departureCity", nullable = false)
	private String departureCity;
	
	@Column(name = "departureDate", nullable = false)
	private Date departureDate;
	
	@Column(name = "arrivalCity", nullable = false)
	private String arrivalCity;
	
	@Column(name = "arrivalDate", nullable = false)
	private Date arrivalDate;
	
	/*private City depCity;
	private City arrivCity;*/
	
	public Flight() {
		
	}

	public Flight(int flightNumber, String type, City departureCity, Date departureDate, City arrivalCity, Date arrivalDate) {
		this.flightNumber = flightNumber;
		this.type = type;
		this.departureCity = departureCity.getName();
		this.departureDate = departureDate;
		this.arrivalCity = arrivalCity.getName();
		this.arrivalDate = arrivalDate;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDepartureCity() {
		return this.departureCity;
	}

	public void setDepartureCityName(String departureCity) {
		this.departureCity = departureCity;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCityName(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	@Override
	public String toString() {
		return "Flight: flightNumber=" + flightNumber + ", type=" + type + ", departureCity=" + departureCity
				+ ", departureDate=" + departureDate + ", arrivalCity=" + arrivalCity + ", arrivalDate=" + arrivalDate;
	}

	
}
