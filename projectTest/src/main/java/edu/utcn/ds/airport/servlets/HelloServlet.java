package edu.utcn.ds.airport.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.utcn.ds.airport.models.Administrator;
import edu.utcn.ds.airport.models.Client;
import edu.utcn.ds.airport.services.LoginService;

/**
 * Servlet implementation class HelloServlet
 */

@WebServlet("/HelloServlet")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("yee");
		LoginService login =  new LoginService();
		if(request.getParameter("userType").equals("client")) {
			Client c= login.findClient(request.getParameter("userName"), request.getParameter("password"));
			if(c != null) {
				System.out.println("yee");
				request.getSession().setAttribute("userName",request.getParameter("userName") );
				request.getSession().setAttribute("userType", "client"); 
				response.sendRedirect("LoginSuccess.jsp");
			}
			else {
				
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/Hello.html");
	            PrintWriter out = response.getWriter();
	            out.println("<font color=red>Either username or password is wrong.</font>");
	            rd.include(request, response);
			}
			
		}
		else {
			Administrator a = login.findAdmin(request.getParameter("userName"), request.getParameter("password"));
			if(a != null) {
				request.getSession().setAttribute("userName",request.getParameter("userName") );
				 request.getSession().setAttribute("userType", "admin");        
	            response.sendRedirect("LoginAdmin.jsp");
			}
			else {
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/Hello.html");
	            PrintWriter out = response.getWriter();
	            out.println("<font color=red>Either username or password is wrong.</font>");
	            rd.include(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
