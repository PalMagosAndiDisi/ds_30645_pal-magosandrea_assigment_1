package edu.utcn.ds.airport.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.utcn.ds.airport.services.FlightServices;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String r = "fail";
		if (request.getParameter("add") != null) {
	           r=add(request);
	        } else if (request.getParameter("edit") != null) {
	           r = edit(request);
	        } else if (request.getParameter("delete") != null) {
	        	
	        	r=doDelete(request.getParameter("flightNumber"));
	        	
	        }else if(request.getParameter("get") != null){
	        	r = get(request.getParameter("flightNumber"));
	        }
		
		StringBuilder buf = new StringBuilder();
		buf.append("<html>" +
		           "<body background = \"D:\\an4_sem1\\DS_Assigment_01\\ds_30645_pal-magosandrea_assigment_1\\projectTest\\sky.JPG\">" +
				   "<h1 style=\"font-family:Comic Sans Ms; text-align: center; margin-top : 70px\">Status:</h1>"+
				   "<div align = \"center\">"+
					"<textarea rows=\"7\" cols=\"40\" align=\"center\" style=\"font-family:Comic Sans Ms; text-align: center; margin-top : 0px\">"+
				   r+
					"</textarea>"+
				   "</div>"+
					"</body>"+
					"</html>");
		
		
		
		
		String html = buf.toString();
		
        PrintWriter out = response.getWriter();
        out.println(html);
          
	        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub
		
	}
	
	protected String get(String flightNumber) {
		
		
		String response = "delete faid";
		try {
		int index = Integer.parseInt(flightNumber);
		FlightServices getFlight = new FlightServices();
		response = getFlight.getAFlight(index);
		}
		catch(java.lang.NumberFormatException e) {
			System.out.println(e.getMessage());
		}
		
		return response;
	}
	
	protected String add(HttpServletRequest request) {
		
		FlightServices addFlight = new FlightServices();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String insert = "add fail";
		try {
		int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
		String flightType = request.getParameter("flightType");
		String deptCity = request.getParameter("deptCity");
		java.util.Date parsed = dateFormat.parse(request.getParameter("deptDate"));
		Date deptDate = new java.sql.Date(parsed.getTime());
		String arrivCity = request.getParameter("arrivCity");
		parsed = dateFormat.parse(request.getParameter("arrivDate"));
		Date arrivDate = new java.sql.Date(parsed.getTime());
		
		insert = addFlight.addFlight(flightNumber, flightType, deptCity, deptDate, arrivCity, arrivDate);
		
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return insert;
		
	}
	
	protected String edit(HttpServletRequest request) {
		FlightServices editFlight = new FlightServices();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String response = "UPDATE fail";
		try {
		int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
		String flightType = request.getParameter("flightType");
		String deptCity = request.getParameter("deptCity");
		java.util.Date parsed = dateFormat.parse(request.getParameter("deptDate"));
		Date deptDate = new java.sql.Date(parsed.getTime());
		String arrivCity = request.getParameter("arrivCity");
		parsed = dateFormat.parse(request.getParameter("arrivDate"));
		Date arrivDate = new java.sql.Date(parsed.getTime());
		
		response = editFlight.editFlight(flightNumber, flightType, deptCity, deptDate, arrivCity, arrivDate);
	
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return response;
	}
	
	protected String doDelete(String flightNumber) {
		String response = "delete faid";
		try {
		int index = Integer.parseInt(flightNumber);
		FlightServices deleteFlight = new FlightServices();
		response = deleteFlight.deleteFlight(index);
		}
		catch(java.lang.NumberFormatException e) {
			System.out.println(e.getMessage());
		}
		
		return response;
	}

}
