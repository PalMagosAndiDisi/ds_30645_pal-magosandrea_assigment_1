package edu.utcn.ds.airport.dao;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import edu.utcn.ds.airport.models.Flight;

public class FligthDao {
	
	public Flight addFlight(int flightNumber, String flightType,String deptCity, Date deptTime, String arrivCity, Date arrivTime) {
		
		Flight f = new Flight();
		f.setFlightNumber(flightNumber);
		f.setType(flightType);
		f.setDepartureCityName(deptCity);
		f.setDepartureDate(deptTime);
		f.setArrivalCityName(arrivCity);
		f.setArrivalDate(arrivTime);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		int id = (Integer)session.save(f);
		f.setFlightId(id);
        session.getTransaction().commit();
		return f;
	}
	
public Flight editFlight(Flight f, String flightType,String deptCity, Date deptTime, String arrivCity, Date arrivTime) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.evict(f);
	
		f.setType(flightType);
		f.setDepartureCityName(deptCity);
		f.setDepartureDate(deptTime);
		f.setArrivalCityName(arrivCity);
		f.setArrivalDate(arrivTime);
		
		session.update(f);
        session.getTransaction().commit();
		return f;
	}
	
	
	public Flight  deleteFlight(Flight flight) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
				session.delete(flight);
			tx.commit();			
		}
		catch (HibernateException e) {
			if (tx != null) {
			tx.rollback();
			}
		} finally {
			session.close();
		}
		return flight;
	}
	
	public List<Flight> findFlights() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		List<Flight> flights = null;
		try {
			session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
		} finally {
			session.close();
		}
		return flights;
	}
	
	
	public Flight findFlight(int flightNumber) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Flight> flights = null;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE  flightnumber= "+flightNumber);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}
	
	
	

}
