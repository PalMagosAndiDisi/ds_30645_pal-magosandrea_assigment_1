package edu.utcn.ds.airport.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import edu.utcn.ds.airport.models.Administrator;
import edu.utcn.ds.airport.models.Client;
import edu.utcn.ds.airport.models.Flight;

public class UserDao {
	
	public Client getClient(String userName, String password) {
	Session session = HibernateUtil.getSessionFactory().openSession();
	List<Client> clients = null;
	Transaction tx = null;
	try {
		tx = session.beginTransaction();
		userName = "'"+userName+"'";
		password = "'"+password+"'";
		Query query = session.createQuery("FROM Client WHERE  password="+userName+" And password= "+ password);
		clients = query.list();
		tx.commit();
	} catch (HibernateException e) {
		if (tx != null) {
			tx.rollback();
		}
		
	} finally {
		session.close();
	}
	return clients != null && !clients.isEmpty() ? clients.get(0) : null;
	}
	
	public Administrator getAdmin(String userName, String password) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Administrator> admins = null;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			userName = "'"+userName+"'";
			password = "'"+password+"'";
			Query query = session.createQuery("FROM Administrator WHERE  password="+userName+" And password= "+ password);
			admins = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			
		} finally {
			session.close();
		}
		return admins != null && !admins.isEmpty() ? admins.get(0) : null;
		}
}
