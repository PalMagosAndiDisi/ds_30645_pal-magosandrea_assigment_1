package edu.utcn.ds.airport.models;

public class City {

	private String name;
	private long latitudeCoordonate;
	private long longitudeCoordonate;
	
	public City() {
		
	}

	public City(String name, long latitudeCoordonate, long longitudeCoordonate) {
		this.name = name;
		this.latitudeCoordonate = latitudeCoordonate;
		this.longitudeCoordonate = longitudeCoordonate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getLatitudeCoordonate() {
		return latitudeCoordonate;
	}

	public void setLatitudeCoordonate(long latitudeCoordonate) {
		this.latitudeCoordonate = latitudeCoordonate;
	}

	public long getLongitudeCoordonate() {
		return longitudeCoordonate;
	}

	public void setLongitudeCoordonate(long longitudeCoordonate) {
		this.longitudeCoordonate = longitudeCoordonate;
	}
	
}
