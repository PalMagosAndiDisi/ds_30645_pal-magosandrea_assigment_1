package edu.utcn.ds.airport.services;

import edu.utcn.ds.airport.dao.FligthDao;
import edu.utcn.ds.airport.dao.UserDao;
import edu.utcn.ds.airport.models.Administrator;
import edu.utcn.ds.airport.models.Client;
import edu.utcn.ds.airport.models.Flight;

public class LoginService {

	public Client findClient(String userName, String password) {
		UserDao dao = new UserDao();
		Client c = dao.getClient(userName, password);
		
		return c;
	}
	
	public Administrator findAdmin(String userName, String password) {
		UserDao dao = new UserDao();
		Administrator a = dao.getAdmin(userName, password);
		
		return a;
	}
}
